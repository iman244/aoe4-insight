import {
  Center,
  Spinner,
  Table,
  TableContainer,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";
import { useContext } from "react";
import { DeleteChartForm } from "./DeleteChartForm";
import { ChartsListContext } from "./ChartsListContextProvider";

export const ChartsList = () => {
  const { charts, isLoading, isError } = useContext(ChartsListContext);

  if (isLoading) {
    return (
      <Center>
        <Spinner />
      </Center>
    );
  }

  if (isError) {
    return `error in ChartsList`;
  }

  return (
    <TableContainer>
      <Table>
        <Thead>
          <Tr>
            <Th>name</Th>
            <Th>remove</Th>
          </Tr>
        </Thead>
        <Tbody>
          {charts?.map((chart) => (
            <Tr key={chart.id}>
              <Td>{chart.name}</Td>
              <Td>
                <DeleteChartForm chart={chart} />
              </Td>
            </Tr>
          ))}
        </Tbody>
      </Table>
    </TableContainer>
  );
};
