import jsonServerApi from "@/app/utilities/jsonServerApi";
import chart_json from "@/types/aoe4_api/entities/chart/chart_json";
import { Button, useToast } from "@chakra-ui/react";
import { useMutation } from "@tanstack/react-query";
import { FormEvent, useContext, useState } from "react";
import { ChartsListContext } from "./ChartsListContextProvider";

export const DeleteChartForm = ({ chart }: { chart: chart_json }) => {
  const query_path: string = "charts";
  const toast = useToast({
    position: "bottom",
  });
  const { refresh_charts_query } = useContext(ChartsListContext);

  const [isLoading, setIsLoading] = useState(false);

  const deleteForm = useMutation({
    mutationKey: [`${query_path}_${chart.id}`],
    mutationFn: async () => {
      return await jsonServerApi.delete(`/${query_path}/${chart.id}`);
    },
    onSuccess: (data) => {
      // show success to user
      toast({
        status: "success",
        description: `${chart.name} chart removed successfully`,
      });

      // reset charts query
      refresh_charts_query && refresh_charts_query();
    },
  });

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    // show loading to indicate start of operation
    setIsLoading(true);

    // start operation
    deleteForm.mutate();
  };

  return (
    <form onSubmit={handleSubmit}>
      <Button
        isLoading={isLoading}
        isDisabled={isLoading}
        type="submit"
        variant={"link"}
        colorScheme="red"
      >
        remove
      </Button>
    </form>
  );
};
