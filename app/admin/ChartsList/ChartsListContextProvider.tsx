import jsonServerApi from "@/app/utilities/jsonServerApi";
import chart_json from "@/types/aoe4_api/entities/chart/chart_json";
import { UseQueryResult, useQuery } from "@tanstack/react-query";
import { AxiosResponse } from "axios";
import { createContext, useEffect, useState } from "react";

export const ChartsListContext = createContext<{
  // Query?: UseQueryResult<AxiosResponse<chart_json[], any>, Error>;
  charts: chart_json[],
  isLoading: boolean;
  isError: boolean;
  refreshToken: number;
  refresh_charts_query?: () => void;
  chart?: (id: number) => chart_json | undefined;
  chart_name?: (name: string) => chart_json | undefined;
}>({
  charts: [],
  refreshToken: 1,
  isLoading: true,
  isError: false,
});

export const ChartsListContextProvider = ({ children }: { children: any }) => {
  const [refreshToken, setRefreshToken] = useState(1);
  const refresh_charts_query = () => setRefreshToken((pre) => pre + 1);
  const [charts, setCharts] = useState<chart_json[]>([]);

  const query_path: string = "charts";

  const Query = useQuery<
    AxiosResponse<chart_json[], any>,
    Error,
    AxiosResponse<chart_json[], any>,
    [string, number]
  >({
    queryKey: [query_path, refreshToken],
    queryFn: async () => {
      return await jsonServerApi(`/${query_path}`);
    },
  });

  // setCharts
  useEffect(() => {
    if (Query.data != undefined) {
      setCharts(Query.data.data);
    }
  }, [Query.data]);

  const chart = (id: number) => {
    return charts.find((c) => c.id == id);
  };

  const chart_name = (name: string) => {
    return charts.find((c) => c.name == name);
  };

  return (
    <ChartsListContext.Provider
      value={{ charts, isLoading: Query.isLoading, isError: Query.isError, refreshToken, refresh_charts_query, chart, chart_name }}
    >
      {children}
    </ChartsListContext.Provider>
  );
};
