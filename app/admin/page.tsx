"use client";
import { Tab, TabList, TabPanel, TabPanels, Tabs } from "@chakra-ui/react";
import { NewChart } from "./NewChart/NewChart";
import { ChartsList } from "./ChartsList/ChartsList";
import { ChartsListContextProvider } from "./ChartsList/ChartsListContextProvider";

export default function Admin() {
  return (
    <ChartsListContextProvider>
      <Tabs colorScheme="yellow">
        <TabList>
          <Tab>Charts List</Tab>
          <Tab>New Chart</Tab>
        </TabList>

        <TabPanels>
          <TabPanel>
            <ChartsList />
          </TabPanel>
          <TabPanel>
            <NewChart />
          </TabPanel>
        </TabPanels>
      </Tabs>
    </ChartsListContextProvider>
  );
}
