import building from "@/types/aoe4_api/entities/entity/building";
import { useQuery } from "@tanstack/react-query";
import { AxiosResponse } from "axios";
import jsonServerApi from "../../utilities/jsonServerApi";
import { Center, Spinner } from "@chakra-ui/react";
import EntitiesTable from "./EntitiesTable";

export default function Buildings() {
  const Query = useQuery<
  AxiosResponse<building[], any>,
  Error,
  AxiosResponse<building[], any>,
  [string]
>({
  queryKey: ["buildings"],
  queryFn: async () => {
    return await jsonServerApi("/buildings");
  },
});

if (Query.isLoading) {
  return (
    <Center>
      <Spinner />
    </Center>
  );
}

if (Query.isError) {
  return "error for /buildings";
}

  if(Query.data) {
    return (
      <EntitiesTable data={Query.data.data} />
    )
  }
}
