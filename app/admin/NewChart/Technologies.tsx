import technology from "@/types/aoe4_api/entities/entity/technology";
import jsonServerApi from "../../utilities/jsonServerApi";
import { useQuery } from "@tanstack/react-query";
import { AxiosResponse } from "axios";
import { Center, Spinner } from "@chakra-ui/react";
import EntitiesTable from "./EntitiesTable";

export default function Technologies() {
  const Query = useQuery<
  AxiosResponse<technology[], any>,
  Error,
  AxiosResponse<technology[], any>,
  [string]
>({
  queryKey: ["technologies"],
  queryFn: async () => {
    return await jsonServerApi("/technologies");
  },
});

if (Query.isLoading) {
  return (
    <Center>
      <Spinner />
    </Center>
  );
}

if (Query.isError) {
  return "error for /technologies";
}

  if(Query.data) {
    return (
      <EntitiesTable data={Query.data.data} />
    )
  }
}
