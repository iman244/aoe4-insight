import React from 'react'
import { NewChartContextProvider } from './NewChartContextProvider'
import { Center, Tab, TabList, TabPanel, TabPanels, Tabs } from '@chakra-ui/react'
import { CreateChartsForm } from './CreateChartsForm'
import Abilities from './Abilities'
import Buildings from './Buildings'
import Technologies from './Technologies'
import Units from './Units'
import Upgrades from './Upgrades'

export const NewChart = () => {
  return (
    <NewChartContextProvider>
    <Center flexDir={"column"} padding={"24px"} gap={"24px"}>
      <CreateChartsForm />
      <Tabs w={"100%"} isFitted >
        <TabList>
          <Tab>Abilities</Tab>
          <Tab>Buildings</Tab>
          <Tab>Technologies</Tab>
          <Tab>Units</Tab>
          <Tab>Upgrades</Tab>
        </TabList>

        <TabPanels>
          <TabPanel>
            <Abilities />
          </TabPanel>
          <TabPanel>
            <Buildings />
          </TabPanel>
          <TabPanel>
            <Technologies />
          </TabPanel>
          <TabPanel>
            <Units />
          </TabPanel>
          <TabPanel>
            <Upgrades />
          </TabPanel>
        </TabPanels>
      </Tabs>
    </Center>
  </NewChartContextProvider>
  )
}
