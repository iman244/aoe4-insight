import abilitiy from "@/types/aoe4_api/entities/entity/abilitiy";
import { useQuery } from "@tanstack/react-query";
import { AxiosResponse } from "axios";
import jsonServerApi from "../../utilities/jsonServerApi";
import { Center, Spinner } from "@chakra-ui/react";
import EntitiesTable from "./EntitiesTable";

export default function Abilities() {

  const Query = useQuery<
  AxiosResponse<abilitiy[], any>,
  Error,
  AxiosResponse<abilitiy[], any>,
  [string]
>({
  queryKey: ["abilities"],
  queryFn: async () => {
    return await jsonServerApi("/abilities");
  },
});

if (Query.isLoading) {
  return (
    <Center>
      <Spinner />
    </Center>
  );
}

if (Query.isError) {
  return "error for /abilities";
}

  if(Query.data) {
    return (
      <EntitiesTable data={Query.data.data} />
    )
  }
}
