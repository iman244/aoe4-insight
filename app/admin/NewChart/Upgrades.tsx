import upgrade from "@/types/aoe4_api/entities/entity/upgrade";
import { useQuery } from "@tanstack/react-query";
import { AxiosResponse } from "axios";
import jsonServerApi from "../../utilities/jsonServerApi";
import { Center, Spinner } from "@chakra-ui/react";
import EntitiesTable from "./EntitiesTable";

export default function Upgrades() {
  const Query = useQuery<
  AxiosResponse<upgrade[], any>,
  Error,
  AxiosResponse<upgrade[], any>,
  [string]
>({
  queryKey: ["upgrades"],
  queryFn: async () => {
    return await jsonServerApi("/upgrades");
  },
});

if (Query.isLoading) {
  return (
    <Center>
      <Spinner />
    </Center>
  );
}

if (Query.isError) {
  return "error for /upgrades";
}

  if(Query.data) {
    return (
      <EntitiesTable data={Query.data.data} />
    )
  }
}
