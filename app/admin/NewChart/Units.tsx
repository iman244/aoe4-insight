import { Center, Spinner } from "@chakra-ui/react";
import jsonServerApi from "../../utilities/jsonServerApi";
import { AxiosResponse } from "axios";
import { useQuery } from "@tanstack/react-query";
import EntitiesTable from "./EntitiesTable";
import unit from "@/types/aoe4_api/entities/entity/unit";

export default function Units() {

  const Query = useQuery<
  AxiosResponse<unit[], any>,
  Error,
  AxiosResponse<unit[], any>,
  [string]
>({
  queryKey: ["units"],
  queryFn: async () => {
    return await jsonServerApi("/units");
  },
});

if (Query.isLoading) {
  return (
    <Center>
      <Spinner />
    </Center>
  );
}

if (Query.isError) {
  return "error for /units";
}

  if(Query.data) {
    return (
      <EntitiesTable data={Query.data.data} />
    )
  }
}
