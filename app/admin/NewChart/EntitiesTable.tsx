import base_entity from "@/types/aoe4_api/entities/entity/base_entity";
import {
  Button,
  Center,
  Checkbox,
  CheckboxGroup,
  Flex,
  IconButton,
  Radio,
  RadioGroup,
  Spinner,
  Table,
  TableContainer,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";
import { difference, intersection, union } from "lodash";
import { useContext, useEffect, useState } from "react";
import { IoIosAdd, IoIosRemove } from "react-icons/io";
import { NewChartContext } from "./NewChartContextProvider";

export default function EntitiesTable({ data }: { data: base_entity[] }) {
  const [typeOfFilter, setTypeOfFilter] = useState("share");
  const [selectedClasses, setSelectedClasses] = useState<(string | number)[]>(
    []
  );
  const classes = union(...data.map((i) => i.classes)).sort();
  const [allAction, setAllAction] = useState("add");
  const {
    new_chart_entities,
    is_in_chart,
    add_entity_to_new_chart,
    remove_entity_from_new_chart,
  } = useContext(NewChartContext);

  if (
    !is_in_chart ||
    !add_entity_to_new_chart ||
    !remove_entity_from_new_chart
  ) {
    return (
      <Center>
        <Spinner />
      </Center>
    );
  }

  const tableData = data.filter((item) => {
    if (selectedClasses.length != 0) {
      if (typeOfFilter == "share") {
        return intersection(item.classes, selectedClasses).length > 0;
      } else if (typeOfFilter == "exact") {
        return (
          intersection(item.classes, selectedClasses).length ==
          item.classes.length
        );
      }
    } else {
      return true;
    }
  });

  const AddAll = () => {
    if (add_entity_to_new_chart) {
      // add each entity in table to chart
      tableData.forEach((new_entity) => add_entity_to_new_chart(new_entity));
    }
  };

  const RemoveAll = () => {
    if (remove_entity_from_new_chart) {
      // remove each entity in table from chart
      tableData.forEach((new_entity) =>
        remove_entity_from_new_chart(new_entity)
      );
    }
  };

  return (
    <Flex
      flexDir={"column"}
      gap={"24px"}
      alignItems={"center"}
      justifyContent={"center"}
      w={"100%"}
    >
      <RadioGroup onChange={setTypeOfFilter} value={typeOfFilter}>
        <Flex gap={"24px"}>
          <Radio value="share">share</Radio>
          <Radio value="exact">exact</Radio>
        </Flex>
      </RadioGroup>
      <Flex gap={"24px"} flexWrap={"wrap"}>
        <CheckboxGroup onChange={setSelectedClasses}>
          {classes.map((c) => (
            <Checkbox key={c} value={c}>
              {c}
            </Checkbox>
          ))}
        </CheckboxGroup>
      </Flex>
      <TableContainer w={"100%"}>
        <Table>
          <Thead>
            <Tr>
              <Th>name</Th>
              <Th>classes</Th>
              <Th>
                <Flex gap={"8px"} alignItems={'center'} justifyContent={'center'}>
                  <Button
                    size={"sm"}
                    onClick={AddAll}
                    colorScheme={"green"}
                  >{`add all`}</Button>
                  <Button
                    size={"sm"}
                    onClick={RemoveAll}
                    colorScheme={"red"}
                  >{`remove all`}</Button>
                </Flex>
              </Th>
            </Tr>
          </Thead>
          <Tbody>
            {tableData.map((entity: any) => (
              <Tr key={entity.id}>
                <Td>{entity.name}</Td>
                <Td>{entity.classes.join(", ")}</Td>
                <Td>
                  <Flex alignItems={"center"} justifyContent={"center"}>
                    {is_in_chart(entity) ? (
                      <IconButton
                        aria-label="remove"
                        icon={<IoIosRemove size={"md"} />}
                        size={"sm"}
                        onClick={() => remove_entity_from_new_chart(entity)}
                        colorScheme="red"
                      />
                    ) : (
                      <IconButton
                        aria-label="add"
                        icon={<IoIosAdd size={"md"} />}
                        size={"sm"}
                        onClick={() => add_entity_to_new_chart(entity)}
                        colorScheme="green"
                      />
                    )}
                  </Flex>
                </Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      </TableContainer>
    </Flex>
  );
}
