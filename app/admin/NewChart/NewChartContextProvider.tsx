import base_entity from "@/types/aoe4_api/entities/entity/base_entity";
import { Dispatch, SetStateAction, createContext, useState } from "react"


export const NewChartContext = createContext<{
    new_chart_entities: base_entity[],
    is_in_chart?: (new_entity: base_entity) => boolean;
    add_entity_to_new_chart?: (new_entity: base_entity)=> void
    remove_entity_from_new_chart?: (new_entity: base_entity)=> void
}>({
  new_chart_entities: []
})

export const NewChartContextProvider = ({children}: {children: any}) => {
    const [new_chart_entities, set_new_chart_entities] = useState<base_entity[]>([]);

    const is_in_chart =  (entity: base_entity) => {
      return !!new_chart_entities.find((i)=>i.id == entity.id)
    }

    const add_entity_to_new_chart = (new_entity: base_entity) => {
      // if it was not in chart, we will add it
      if(!is_in_chart(new_entity)) {
        set_new_chart_entities(pre=>[...pre,new_entity])
      }
    }

    const remove_entity_from_new_chart = (new_entity: base_entity) => {
      // if it was in chart, we will remove it
      if(is_in_chart(new_entity)) {
        set_new_chart_entities(pre => {
          const new_list = pre.filter((i)=>i.id !== new_entity.id)
          return new_list
        })
      }
    }

    return (
      <NewChartContext.Provider value={{new_chart_entities: new_chart_entities, is_in_chart, add_entity_to_new_chart, remove_entity_from_new_chart}}>
        {children}
      </NewChartContext.Provider>
    );
}
