import { Button, Flex, Heading, Input, Text, useToast } from "@chakra-ui/react";
import { FormEvent, useContext, useRef, useState } from "react";
import { AoeHeading } from "../../components/AoeHeading";
import { NewChartContext } from "./NewChartContextProvider";
import { useMutation } from "@tanstack/react-query";
import jsonServerApi from "../../utilities/jsonServerApi";
import { union } from "lodash";
import { ChartsListContext } from "../ChartsList/ChartsListContextProvider";
import chart from "@/types/aoe4_api/entities/chart/chart";
import chart_detail from "@/types/aoe4_api/entities/chart/chart_detail";


export const CreateChartsForm = () => {
  const toast = useToast();
  const { new_chart_entities, remove_entity_from_new_chart } =
    useContext(NewChartContext);
  const { refresh_charts_query } = useContext(ChartsListContext);
  const [chart_name, setChart_name] = useState<string>();
  const [isInvalid, setIsInvalid] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const chart_name_input_ref = useRef<HTMLInputElement | null>(null);

  const newChartMutation = useMutation({
    mutationKey: ["newChart"],
    mutationFn: async (data: chart) => {
      return await jsonServerApi.post("/charts", data);
    },
    onSuccess: () => {
      // refresh charts query
      refresh_charts_query && refresh_charts_query();

      // show success to user
      toast({
        description: `${chart_name} chart added successfully`,
        position: "bottom",
        status: "success",
      });

      // reset the form
      setIsLoading(false);
      setChart_name("");
      !!remove_entity_from_new_chart &&
        new_chart_entities.forEach((entity) =>
          remove_entity_from_new_chart(entity)
        );
    },
  });

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    var errors = [];

    // error check for chart name
    if (chart_name == undefined || chart_name == "") {
      errors.push("chart_name");

      // show response to user
      setIsInvalid(true);
      chart_name_input_ref.current?.focus();
      toast({
        status: "error",
        position: "bottom",
        description: "please add a name for new chart",
      });
    }
    // error check for new chart items
    if (new_chart_entities.length == 0) {
      errors.push("chart_items");

      // show response to user
      toast({
        status: "error",
        position: "bottom",
        description: "please add at least one entity to new chart",
      });
    }

    // if no error find, we will add new chart
    if (errors.length == 0) {
      // we show loading to user to indicate start of operation
      setIsLoading(true);

      // cooking data to post
      var cooked_data: chart = {
        // we use ! for chart_name beacuse we check its undefined condition in error checking section
        name: chart_name!,
        details: [],
      };

      // -- gather pbgid list for each entity
      var details_lists: chart_detail[][] = new_chart_entities.map((entity) => {
        const single_variation_detail: chart_detail[] =  entity.variations.map((base_variation) => ({
          pbgid: base_variation.pbgid,
          costs: base_variation.costs ? base_variation.costs : entity.costs
        }));
        return single_variation_detail
      });
      //   return entity.variations.map((base_variation) => {
      //     pbgid: base_variation.pbgid,
      //     cost: entity
      //   });
      // });

      // -- we make a one list that have all the desired pbgids
      const pbgid_list = union(...details_lists);
      cooked_data.details = pbgid_list;

      // send cooked data to react-query phase
      newChartMutation.mutate(cooked_data);
    }
  };

  return (
    <form
      style={{
        width: "fit-content",
        padding: "24px",
        border: "1px solid gray",
        display: "flex",
        flexDirection: "column",
        gap: "24px",
        justifyContent: "center",
        alignItems: "center",
      }}
      onSubmit={handleSubmit}
    >
      <AoeHeading w={"fit-content"} textAlign={"center"}>
        new chart
      </AoeHeading>
      <Flex gap={"24px"} flexDir={"column"}>
        <Input
          ref={chart_name_input_ref}
          isInvalid={isInvalid}
          focusBorderColor="#ffdf91"
          variant={"flushed"}
          w={"200px"}
          value={chart_name}
          onChange={(e) => {
            setChart_name(e.target.value);
            if (e.target.value != "") {
              setIsInvalid(false);
            }
          }}
          placeholder="chart name"
        />
        <Flex flexDir={"column"} gap={"8px"}>
          <Heading
            w={"fit-content"}
            size={"sm"}
            borderBottom={"1px solid gray"}
            pb={"4px"}
          >
            items to merge
          </Heading>
          {new_chart_entities.map((new_entity) => (
            <Flex
              key={new_entity.id}
              alignItems={"center"}
              justifyContent={"space-between"}
            >
              <Text>{new_entity.name}</Text>
              <Button
                size={"sm"}
                variant={"link"}
                colorScheme="red"
                fontSize={"small"}
                onClick={() => {
                  if (remove_entity_from_new_chart) {
                    remove_entity_from_new_chart(new_entity);
                  }
                }}
              >
                remove
              </Button>
            </Flex>
          ))}
          {new_chart_entities.length == 0 && (
            <Text>no items have been added</Text>
          )}
        </Flex>
      </Flex>
      <Button
        type="submit"
        w={"fit-content"}
        colorScheme="blue"
        isLoading={isLoading}
        isDisabled={isLoading}
      >
        Create
      </Button>
    </form>
  );
};
