import { Text, TextProps } from "@chakra-ui/react";
import React from "react";

export const AoeHeading = (props: TextProps) => {
  const { children, ...rest } = props;
  return (
    <Text
      color={"#ffdf91"}
      style={{
        borderImage:
          "linear-gradient(90deg, rgba(184, 134, 45, 0), #b8862d 15%, #ffdf91 50%, #b8862d 85%, rgba(184, 134, 45, 0)",
        borderWidth: "0 0 2px",
        borderStyle: "solid",
        borderImageSlice: 2,
      }}
      textTransform={'capitalize'}
      {...rest}
    >
      {children}
    </Text>
  );
};
