import { GroupBase, StylesConfig, ThemeConfig } from "react-select"

const aoe4SelectTheme: ThemeConfig  = (theme) => {
    return {
        ...theme,
        colors: {
            ...theme.colors,
            neutral0: '#181c29',
            primary: "#e7c067",
        }
    }
}

export default aoe4SelectTheme