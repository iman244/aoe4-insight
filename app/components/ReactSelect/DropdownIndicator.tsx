import { Center, Icon } from "@chakra-ui/react";
import Image from "next/image";

export default function DropdownIndicator() {
  return (
    <Center padding={'8px'}>
        <Image src={"/ui/arrow_down.svg"} alt="arrow" width={24} height={24} />
    </Center>
  )
}
