import { GroupBase, StylesConfig, ThemeConfig } from "react-select";

const aoe4SelectStyle: StylesConfig<
  { value: string; label: string },
  false,
  GroupBase<{ value: string; label: string }>
> = {
  control(base, props) {
    return {
      ...base,
      background: "linear-gradient(#394766, #1e2536)",
      minWidth: '200px'
    };
  },
  indicatorSeparator(base, props) {
    return {
      display: "none",
    };
  },
  option(base, props) {
    return {
      ...base,
      color: "#e7c067",
      background: "#181c29",
      ":hover": {
        background: "#394766",
        cursor: "pointer",
      },
    };
  },
  menu(base, props) {
    return {
      ...base,
      border: "1px solid gray",
      zIndex: 9999,
    };
  },
  singleValue(base, props) {
    return {
      ...base,
      color: "#e7c067",
    };
  },
  multiValue(base, props) {
    return {
      ...base,
      color: "#e7c067",
      background: "gray",
    };
  },
  multiValueLabel(base, props) {
    return {
      ...base,
      color: "#e7c067",
    };
  },
  input(base, props) {
    return {
      ...base,
      color: "#e7c067",
    };
  },
  placeholder(base, props) {
    return {
      ...base,
      color: "#e7c067",
    };
  },
};

export default aoe4SelectStyle;
