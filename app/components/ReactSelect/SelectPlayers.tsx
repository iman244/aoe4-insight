import Select, { GroupBase, OptionsOrGroups } from "react-select";
import aoe4SelectStyle from "./aoe4SelectStyle";
import aoe4SelectTheme from "./aoe4SelectTheme";
import { multiSelectState } from "@/app/games/[profile_id]/[game_id]/page";
import player_replay_summary from "@/types/aoe4_api/entities/summary/player_replay_summary";
import { Checkbox, CheckboxGroup } from "@chakra-ui/react";

export const SelectPlayers = ({
  players,
  selectPlayerState,
}: {
  players: player_replay_summary[];
  selectPlayerState: multiSelectState;
}) => {
  const react_select_players = players.map((p) => ({
    value: `${p.profileId}`,
    label: p.name,
  }));

  return (
    <Select
      name="players"
      // @ts-ignore
      isMulti
      styles={aoe4SelectStyle}
      theme={aoe4SelectTheme}
      placeholder={"Select Players"}
      options={react_select_players}
      // @ts-ignore
      onChange={selectPlayerState[1]}
    />
  );
};

//   <CheckboxGroup>
//   {react_select_players.map((p)=><Checkbox key={p.value} value={p.value}>{p.label}</Checkbox>)}
// </CheckboxGroup>
