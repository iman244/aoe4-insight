import Select from "react-select";
import aoe4SelectStyle from "./aoe4SelectStyle";
import aoe4SelectTheme from "./aoe4SelectTheme";
import * as _ from "lodash";
import { useQuery } from "@tanstack/react-query";
import { AxiosResponse } from "axios";
import jsonServerApi from "@/app/utilities/jsonServerApi";
import { Spinner } from "@chakra-ui/react";
import { Dispatch, SetStateAction } from "react";
import json_chart from "@/types/aoe4_api/entities/chart/chart_json";
import chart_json from "@/types/aoe4_api/entities/chart/chart_json";

export const SelectCharts = ({
  selectedChartState,
}: {
  selectedChartState: [
    (
      | {
          value: string;
          label: string;
        }
      | null
    ),
    Dispatch<
      SetStateAction<
        | {
            value: string;
            label: string;
          }
        | null
      >
    >
  ];
}) => {
  const chartsQuery = useQuery<
    AxiosResponse<chart_json[], any>,
    Error,
    AxiosResponse<chart_json[], any>,
    [string]
  >({
    queryKey: ["charts"],
    queryFn: async () => {
      return await jsonServerApi("/charts");
    },
  });

  if (chartsQuery.isLoading) {
    return <Spinner />;
  }

  if (chartsQuery.isError) {
    return "error for /charts";
  }

  return (
    <>
      {chartsQuery.data && (
        <Select
          name="charts"
          styles={aoe4SelectStyle}
          theme={aoe4SelectTheme}
          placeholder={"Select a chart"}
          options={chartsQuery.data.data.map((chart) => ({
            label: chart.name,
            value: `${chart.id}`,
          }))}
          value={selectedChartState[0]}
          //@ts-ignore
          onChange={selectedChartState[1]}
        />
      )}
    </>
  );
};
