import { extendTheme } from "@chakra-ui/react";

export default extendTheme({
  styles: {
    global: {
      "html, body": {
        color: "white",
        background: "#181c29",
      },
    },
  },
});
