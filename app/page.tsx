"use client";
import { search_player_api } from "@/types/aoe4_api/api_list/search_player";
import { Center, Flex, Input, Text } from "@chakra-ui/react";
import { useQuery } from "@tanstack/react-query";
import axios, { AxiosResponse } from "axios";
import Link from "next/link";
import { useState } from "react";

export default function Home() {
  const [search, setSearch] = useState("");

  const searchQuery = useQuery<
    AxiosResponse<search_player_api, any>,
    Error,
    AxiosResponse<search_player_api, any>,
    string[]
  >({
    queryKey: ["search", search],
    queryFn: async ({ queryKey }) => {
      const [_key, search] = queryKey;
      return await axios.get(
        `https://aoe4world.com/api/v0/players/search?query=${search}`
        // `https://aoe4world.com/api/v0/players/autocomplete?query=${search}&limit=10&leaderboard=rm_solo&page=${page}`
      );
    },
  });

  return (
    <Flex
      justifyContent={"center"}
      padding={"24px"}
      flexDir={"column"}
      gap={"12px"}
    >
      <Input
        value={search}
        onChange={(e) => setSearch(e.target.value)}
        placeholder="search for aoe players"
      />
      {searchQuery.data?.data.players.map((player, index) => {
        return (
          <Link href={`/games/${player.profile_id}`} key={index}>
            {player.name}
          </Link>
        );
      })}
      {search.length != 0 && searchQuery.data?.data.count == 0 && (
        <Text>no player found!</Text>
      )}
    </Flex>
  );
}
