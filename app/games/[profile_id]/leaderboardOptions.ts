import { GroupBase, OptionsOrGroups } from "react-select";

const leaderboardOptions: OptionsOrGroups<{
    value: string;
    label: string;
}, GroupBase<{
    value: string;
    label: string;
}>> = [
    {
    value: "rm_solo",
    label: "Solo",

},
    {
    value: "rm_team",
    label: "team",

},
    {
    value: "rm_1v1",
    label: "rm_1v1",

},
    {
    value: "rm_2v2",
    label: "rm_2v2",

},
    {
    value: "rm_3v3",
    label: "rm_3v3",

},
    {
    value: "rm_4v4",
    label: "rm_4v4",

},
    {
    value: "qm_1v1",
    label: "qm_1v1",

},
    {
    value: "qm_2v2",
    label: "qm_2v2",

},
    {
    value: "qm_3v3",
    label: "qm_3v3",

},
    {
    value: "qm_4v4",
    label: "qm_4v4",

},
]

export default leaderboardOptions