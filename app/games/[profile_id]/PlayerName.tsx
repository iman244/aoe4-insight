import { AoeHeading } from "@/app/components/AoeHeading";
import retrive_player_info_from_game from "@/app/utilities/retrive_player_info_from_game";
import game from "@/types/aoe4_api/entities/game/game";
import { Text } from "@chakra-ui/react";

export default function PlayerName({
  profile_id,
  game,
}: {
  profile_id: number;
  game: game;
}) {
  return (
    <AoeHeading fontSize="larger">
      {retrive_player_info_from_game(profile_id, game).name} Games
    </AoeHeading>
  );
}
