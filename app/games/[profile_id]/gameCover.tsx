import retrive_player_info_from_game, { retrive_opponents_info_from_game } from "@/app/utilities/retrive_player_info_from_game";
import game from "@/types/aoe4_api/entities/game/game";
import { Box, Card, Flex } from "@chakra-ui/react";
import Image from "next/image";
import Link from "next/link";

export default function GameCover({
  profile_id,
  game,
}: {
  profile_id: number;
  game: game;
}) {
  const player_in_game_details = retrive_player_info_from_game(
    profile_id,
    game
  );

  const _1v1 = game.kind == "rm_1v1" || game.kind == "qm_1v1";

  if (game.kind == "rm_1v1" || game.kind == "qm_1v1") {
    console.log("game", game);
  }

  const opponent = retrive_opponents_info_from_game(profile_id,game)

  return (
    <Box
      position={"relative"}
      border={"2px solid transparent"}
      as={Link}
      w={"220px"}
      id={`game-${game.game_id}`}
      href={`/games/${profile_id}/${game.game_id}`}
      _hover={{
        borderImageSource: "url(/ui/outline.svg)",
        borderWidth: "2px",
        borderImageSlice: "4",
        borderStyle: "solid",
      }}
    >
      <Image
        src={`/flags/${player_in_game_details.civilization}.webp`}
        alt={player_in_game_details.civilization}
        width={220}
        height={120}
      />
      {_1v1 && (
        <Image
          style={{ position: "absolute", top: 0, right: 0, zIndex: 1 }}
          src={`/flags/${opponent.civilization}.webp`}
          alt={`${game.map}`}
          width={50}
          height={30}
        />
      )}
      <Flex
        alignItems={"center"}
        justifyContent={"space-around"}
        backgroundImage={"url(/ui/textile-60c.webp)"}
        background={
          player_in_game_details.result == "win" ? "green.700" : "red.700"
        }
        height={"30px"}
        color={"#ffdf91"}
        position={"relative"}
      >
        <Image
          style={{ position: "absolute", top: "-50px", right: 0, zIndex: 1 }}
          src={`/maps/${game.map}.png`}
          alt={`${game.map}`}
          width={50}
          height={30}
        />

        <span>{game.kind}</span>
        {player_in_game_details.rating_diff && (
          <span>{player_in_game_details.rating_diff}</span>
        )}
      </Flex>
    </Box>
  );
}
