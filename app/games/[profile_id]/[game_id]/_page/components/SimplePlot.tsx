"use client";
import * as Plot from "@observablehq/plot";
import { useEffect, useRef, useState } from "react";
import { Game } from "../../type";

export function SimplePlot({ game }: { game: Game }) {
  console.log("game", game)
  const containerRef = useRef<HTMLDivElement>(null);
  const [data1, setData1] = useState(
    game.players[0].resources.timestamps.map((n, index) => ({
      seconds: n,
      food: game.players[0].resources["food"][index],
      wood: game.players[0].resources["wood"][index],
      gold: game.players[0].resources["gold"][index],
    }))
  );
  // const [data, setData] = useState(

    type r = {
      second: number;
      resource: "food" | "wood" | "gold" | 'stone';
      value: number;
    }[]
    const resource: r[number]['resource'][] = ["food"];
  const data: r = resource.reduce((pre, curr) => {
    const res_data: r = game.players[0].resources.timestamps.map((s, index) => ({
      second: s,
      resource: curr,
      value: game.players[0].resources[`${curr}Gathered`][index],
    }));
    return [...pre, ...res_data];
  }, [] as r).filter((rec)=>rec.second % 300 == 0);
  
  

  useEffect(() => {
    if (data === undefined) return;
    const plot = Plot.plot({
      y: { grid: true },
      color: {
        domain: ["food", "wood", "gold", "stone"],
        range: ["red", "green", "gold", "gray"],
      },
      marks: [
        Plot.ruleY([0]),
        Plot.barY(
          data,
          Plot.groupX(
            { y: "sum" },
            { x: "second", fill: "resource", y: "value" }
          )
        ),
      ],
    });
    containerRef.current?.append(plot);
    return () => plot.remove();
  }, [data]);

  return <div ref={containerRef} />;
}