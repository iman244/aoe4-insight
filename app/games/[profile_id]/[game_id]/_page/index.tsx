"use client";
import { Box, Center, Container, Spinner, Text } from "@chakra-ui/react";
import { Dispatch, SetStateAction, use } from "react";
import { redirect } from "next/navigation";
import { useIndexdb } from "../hooks/useIndexdb";
import { Status, useGame } from "../hooks/useGame";
import { GameInfo } from "./components/GameInfo";
import { SimplePlot } from "./components/SimplePlot";

export type multiSelect = { value: string; label: string }[];
export type multiSelectState = [
  multiSelect,
  Dispatch<SetStateAction<multiSelect>>
];

export default function GamePage(props: {
  params: Promise<{ game_id: string; profile_id: string }>;
}) {
  const params = use(props.params);
  const { game_id, profile_id } = params;

  const log = process.env.NEXT_PUBLIC_ENVIRONMENT != "production";

  const { db, error, status: db_status } = useIndexdb("aoe4-insight", 2);
  const { game, status: game_status } = useGame(db, {
    game_id,
  });

  const status: Status =
    db_status == "success" && game_status == "success"
      ? "success"
      : db_status == "error" || game_status == "error"
      ? "error"
      : "loading";

  log && console.log("GamePage game", game);

  if (status == "loading") {
    return (
      <Center minH={"300px"}>
        <Spinner />
      </Center>
    );
  }

  if ((status == "success" && game == null) || game == null) {
    log && console.log("status", status);
    log && console.log("game", game);
    redirect("/games" + `/${profile_id}` + `/${game_id}` + "/upload");
  }
  game.players[0].resources.foodGathered;
  return (
    <Container>
      <GameInfo game={game} />
      <Box>
        <SimplePlot game={game} />
      </Box>
    </Container>
  );
}
