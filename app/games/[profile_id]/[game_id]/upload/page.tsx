"use client";

import {
  Button,
  Center,
  Flex,
  Heading,
  Icon,
  Link,
  Text,
  Textarea,
  useToast,
} from "@chakra-ui/react";
import { useRouter } from "next/navigation";
import { FormEventHandler, use, useEffect, useState } from "react";
import { FaExternalLinkAlt } from "react-icons/fa";
import { Game } from "../type";
import { useIndexdb } from "../hooks/useIndexdb";
import { useGame } from "../hooks/useGame";

export const UploadGameDataPage = (props: {
  params: Promise<{ game_id: string; profile_id: string }>;
}) => {
  const params = use(props.params);
  const { game_id, profile_id } = params;

  const log = process.env.NEXT_PUBLIC_ENVIRONMENT != "production";
  const [json, setJson] = useState("");
  const [jsonError, set_jsonError] = useState("");
  const [jsonSuccess, set_jsonSuccess] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const { db, error } = useIndexdb("aoe4-insight", 2);
  const { game: pre_data_game, error: game_error } = useGame(db, {
    game_id,
  });
  const router = useRouter();

  const handleSubmit: FormEventHandler<HTMLFormElement> = (event) => {
    event.preventDefault();

    if (json.length == 0) {
      set_jsonError("Please enter your data");
      return;
    }
    const game: Game = JSON.parse(json);

    if (db == null) {
      set_jsonError("index db dose not openned");
      return;
    }
    setIsLoading(true);

    try {
      const transaction = db.transaction(["games"], "readwrite");
      transaction.oncomplete = (event) => {
        log && console.log("Index db transaction success event: ", event);
        // set_jsonSuccess("Transaction done!");
      };

      transaction.onerror = (event) => {
        log && console.log("Index db transaction Error event: ", event);
        set_jsonError("Index db transaction Error");
        setIsLoading(false);
      };

      const objectStore = transaction.objectStore("games");

      const request = pre_data_game
        ? objectStore.put(game)
        : objectStore.add(game);

      request.onsuccess = (event) => {
        // event.target.result === customer.ssn;
        log && console.log("request.onsuccess event", event);
        set_jsonSuccess("Data added successfully, loading charts...");
        setIsLoading(false);
        router.push("/games" + `/${profile_id}` + `/${game_id}`);
      };

      request.onerror = (event) => {
        log && console.log("request.onerror event", event);
        set_jsonError("Data did not added");
        setIsLoading(false);
      };
    } catch (error) {
      log && console.log("error", error);
      if (error instanceof DOMException) {
        if (error.name == "NotFoundError") {
          db.createObjectStore("summary_game", {
            keyPath: "summary_game",
          });
        }
      }
    }
  };

  return (
    <Center minH={"100vh"} flexDir={"column"} gap={"48px"} padding={"48px"}>
      <Button
        as={Link}
        href={`https://aoe4world.com/players/${profile_id}/games/${game_id}`}
        isExternal
        alignSelf={"center"}
      >
        please retrive and upload game data from aoe4world game page
        <Icon as={FaExternalLinkAlt} boxSize={3} ml={"8px"} />
      </Button>
      {pre_data_game && (
        <Text fontSize={"x-large"} color={"yellow"}>
          Your game is already saved!{" "}
          <Link
            href={"/games" + `/${profile_id}` + `/${game_id}`}
            style={{
              textDecoration: "underline",
            }}
          >
            view your charts
          </Link>
        </Text>
      )}
      <form onSubmit={handleSubmit} style={{ width: "100%" }}>
        <Flex flexDir={"column"} gap={"12px"}>
          <Heading
            borderBottom={"1px solid gray"}
            w={"fit-content"}
            paddingBottom={"4px"}
          >
            Upload json data
          </Heading>
          {jsonError && (
            <Text fontSize={"small"} color={"red.300"}>
              {jsonError}
            </Text>
          )}
          {jsonSuccess && (
            <Text fontSize={"small"} color={"green.300"}>
              {jsonSuccess}
            </Text>
          )}
          <Textarea
            w={"100%"}
            value={json}
            onChange={(e) => setJson(e.target.value)}
            placeholder="please add your json data that you retrive from aoe4world summary page"
          />
          <Button type="submit" isLoading={isLoading} isDisabled={isLoading}>
            {pre_data_game != null ? "Update" : "Save"} my game data
          </Button>
        </Flex>
      </form>
    </Center>
  );
};

export default UploadGameDataPage;
