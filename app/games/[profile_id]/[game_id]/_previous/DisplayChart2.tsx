import { ChartsListContext } from "@/app/admin/ChartsList/ChartsListContextProvider";
import calculate_buildOrder_value from "@/app/utilities/data_cooking/calculate_buildOrder_value";
import data_head from "@/app/utilities/data_cooking/data_head";
import extract_buildOrders from "@/app/utilities/data_cooking/extract_buildOrders";
import extract_display_players_data from "@/app/utilities/data_cooking/extract_display_players_data";
import jsonServerApi from "@/app/utilities/jsonServerApi";
import { areaChartType } from "@/types/aoe4_api/entities/chart/areaChartType";
import chart_json from "@/types/aoe4_api/entities/chart/chart_json";
import { startegy } from "@/types/aoe4_api/entities/chart/startegy";
import { valueMeasureType } from "@/types/aoe4_api/entities/chart/valueMeasureType";
import { valueType } from "@/types/aoe4_api/entities/chart/valueType";
import game_summary from "@/types/aoe4_api/entities/summary/game_replay_summary";
import {
  Center,
  Checkbox,
  CheckboxGroup,
  Flex,
  Heading,
  Radio,
  RadioGroup,
  Spinner,
  Text,
} from "@chakra-ui/react";
import { useQuery } from "@tanstack/react-query";
import { AxiosResponse } from "axios";
import { sum, uniq } from "lodash";
import {
  Dispatch,
  SetStateAction,
  useContext,
  useEffect,
  useState,
} from "react";
import Chart from "react-google-charts";

export const DisplayChart2 = ({
  duration,
  game_data,
  display_chart
}: {
  duration: number;
  game_data: game_summary;
  display_chart: number;
}) => {
  const players_data = game_data.players;
  const players = game_data.players.map((p) => p.name);
  const [display_players, setDisplayPlayers] =
    useState<(string | number)[]>(players);
  const [chartData, setChartData] = useState<(string | number)[][]>([]);
  const [valueType, setValueType] = useState<valueType>("cost");
  const [valueMeasure, setValueMeasure] = useState<valueMeasureType>("balance");
  const [areaChartType, setAreaChartType] = useState<areaChartType>("relative");
  const { charts, isLoading, isError, chart } =
    useContext(ChartsListContext);

  useEffect(() => {
    if (charts.length > 0 && chart != undefined && chart(display_chart) != undefined && display_players.length > 0) {
      
      const chart_details = chart(display_chart)!.details;
      const pbgids_list = chart_details.map((d) => d.pbgid);
      // build head of data
      const data: (string | number)[][] = [data_head(display_players)];

      // for each second calculate value
      for (let i = 0; i <= duration; i++) {
        // for each player calculate value
        const display_players_data = extract_display_players_data(
          players_data,
          display_players
        );

        const chart_values_for_second_i = display_players_data.map(
          (player, index) => {
            // const log = i == 0 && index == 0;

            // find items that we want to calculate
            const buildOrders = extract_buildOrders(
              pbgids_list,
              player.buildOrder
            );

            // for each item calculate the value - count or costs -- value could be count or cost
            const value = calculate_buildOrder_value(
              i,
              buildOrders,
              valueType,
              valueMeasure,
              // for retrive cost of the build order
              chart_details
            );

            // merge the calculated items
            return sum(value);
          }
        );

        // push it to data
        // `${Math.floor(i/60)}:${i % 60}`
        data.push([i, ...chart_values_for_second_i]);
      }

      setChartData(data);
    }
  }, [display_players, charts, valueType, valueMeasure, display_chart]);

  if (!display_players.length) {
    return <Text>please select a player to display</Text>;
  }

  if (isLoading) {
    return (
      <Center>
        <Spinner />
      </Center>
    );
  }

  if (isError) {
    return (
      <Center>
        <Text>error occured in getting charts detail</Text>
      </Center>
    );
  }

  if (charts.length == 0) {
    return (
      <Center>
        <Text>charts had not build successfully</Text>
      </Center>
    );
  }

  return (
    <Flex
      w={"90vw"}
      margin={"24px"}
      background={"linear-gradient(#394766, #1e2536)"}
      flexDir={"column"}
      padding={"0 24px 24px"}
    >
      <Chart
        chartType="AreaChart"
        height="400px"
        data={chartData}
        className="chart"
        options={{
          isStacked: areaChartType == "stack" ? true : areaChartType,
          curveType: "function",
          legend: "bottom",
          backgroundColor: "transparent",
          hAxis: {
            minValue: 0,
            // ticks: timeTick,
            title: "time",
          },
          vAxis: {
            minValue: 0,
            title: `${chart && chart(display_chart)?.name} ${valueMeasure} - ${valueType}`,
          },
        }}
      />
      <Flex alignItems={"center"} justifyContent={"space-between"} mt={"12px"}>
        <DisplayPlayerFilter
          game_data={game_data}
          display_players={display_players}
          setDisplayPlayers={setDisplayPlayers}
        />
        <Flex flexDir={"column"} gap={"12px"}>
          <RadioGroup
            value={valueType}
            onChange={(e: valueType) => setValueType(e)}
          >
            <Flex gap={"24px"}>
              <Text>value type</Text>
              <Radio value="count">count</Radio>
              <Radio value="cost">cost</Radio>
            </Flex>
          </RadioGroup>
          <RadioGroup
            value={valueMeasure}
            onChange={(e: valueMeasureType) => setValueMeasure(e)}
          >
            <Flex gap={"24px"}>
              <Text>value measure</Text>
              <Radio value="balance">balance</Radio>
              <Radio value="product">product</Radio>
              <Radio value="lost">lost</Radio>
            </Flex>
          </RadioGroup>
          <ChartTypeRadio
            areaChartType={areaChartType}
            setAreaChartType={setAreaChartType}
          />
        </Flex>
      </Flex>
    </Flex>
  );
};


const ChartTypeRadio = ({
  areaChartType,
  setAreaChartType,
}: {
  areaChartType: areaChartType;
  setAreaChartType: Dispatch<SetStateAction<areaChartType>>;
}) => {
  return (
    <RadioGroup
      value={areaChartType}
      onChange={(e: areaChartType) => setAreaChartType(e)}
    >
      <Flex gap={"24px"}>
        <Text>value measure</Text>
        <Radio value="stack">stack</Radio>
        <Radio value="relative">relative</Radio>
      </Flex>
    </RadioGroup>
  );
};

const DisplayPlayerFilter = ({
  game_data,
  display_players,
  setDisplayPlayers,
}: {
  game_data: game_summary;
  display_players: (string | number)[];
  setDisplayPlayers: Dispatch<SetStateAction<(string | number)[]>>;
}) => {
  const teams = uniq(game_data.players.map((p) => p.teamName).sort());
  const team_players = teams.map((teamName) => ({
    teamName: teamName,
    players: game_data.players.filter((v) => v.teamName == teamName),
  }));
  return (
    <>
      {/* gap between team columns */}
      <Flex gap={"36px"} flexWrap={"wrap"}>
        <CheckboxGroup
          value={display_players}
          onChange={(e) => {
            if (e.length == 0) {
              setDisplayPlayers(game_data.players.map((p) => p.name));
            } else {
              setDisplayPlayers(e);
            }
          }}
        >
          {/* for each team we have a column */}
          {team_players.map((t) => (
            <Flex key={t.teamName} flexDir={"column"} gap={"12px"}>
              <Heading size={"small"}>{t.teamName}</Heading>
              {t.players.map((p) => (
                <Checkbox key={p.profileId} value={p.name}>
                  {p.name}
                </Checkbox>
              ))}
            </Flex>
          ))}
        </CheckboxGroup>
      </Flex>
    </>
  );
};
