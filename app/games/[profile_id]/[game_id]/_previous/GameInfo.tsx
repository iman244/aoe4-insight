import game_summary from "@/types/aoe4_api/entities/summary/game_replay_summary"
import player_replay_summary from "@/types/aoe4_api/entities/summary/player_replay_summary"
import { Box, Flex, Text } from "@chakra-ui/react"
import Image from "next/image"

export const GameInfo = ({game}: {game: game_summary}) => {
    const winnerTeam = game.players.filter((p)=>p.result == 'win')
    const looserTeam = game.players.filter((p)=>p.result == 'loss')
  return (
    <Flex alignItems={'center'} justifyContent={'center'} gap={'48px'} padding={'24px'}>
        {winnerTeam.map((p)=> <PlayerCard key={p.profileId} player={p} />) }
        <MapCard mapName={game.mapName} />
        {looserTeam.map((p)=> <PlayerCard key={p.profileId} player={p} />) }
    </Flex>
  )
}


const PlayerCard = ({player}: {player: player_replay_summary}) => {
    return <Flex flexDir={'column'}>
        <Image src={`/flags/${player.civilization}.webp`} width={200} height={200} alt={player.civilization} />
        <Text textAlign={'center'} color={"#e7c067"} bgColor={player.result == "win" ? 'green.800' : 'red.800'}>{player.name}</Text>
    </Flex>
}

const MapCard = ({mapName}: {mapName: string}) => {
    return <Flex flexDir={'column'} gap={'8px'} alignItems={'center'} justifyContent={'center'}>
        <Image src={`/maps/${mapName}.png`} width={100} height={100} alt={mapName} />
        <Text textAlign={'center'}>{mapName}</Text>
    </Flex>
}