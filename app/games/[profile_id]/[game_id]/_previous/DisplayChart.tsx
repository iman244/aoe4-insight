import cost_calculation from "@/app/utilities/cost_calculation";
import count_calculation from "@/app/utilities/count_calculation";
import calculate_buildOrder_value from "@/app/utilities/data_cooking/calculate_buildOrder_value";
import data_head from "@/app/utilities/data_cooking/data_head";
import extract_buildOrders from "@/app/utilities/data_cooking/extract_buildOrders";
import extract_display_players_data from "@/app/utilities/data_cooking/extract_display_players_data";
import jsonServerApi from "@/app/utilities/jsonServerApi";
import chart_json from "@/types/aoe4_api/entities/chart/chart_json";
import { valueMeasureType } from "@/types/aoe4_api/entities/chart/valueMeasureType";
import { valueType } from "@/types/aoe4_api/entities/chart/valueType";
import game_summary from "@/types/aoe4_api/entities/summary/game_replay_summary";
import {
  Box,
  Center,
  Checkbox,
  CheckboxGroup,
  Flex,
  Heading,
  Radio,
  RadioGroup,
  Spinner,
  Text,
} from "@chakra-ui/react";
import { useQuery } from "@tanstack/react-query";
import { AxiosResponse } from "axios";
import { sum, union, uniq } from "lodash";
import { Dispatch, SetStateAction, useEffect, useState } from "react";
import Chart from "react-google-charts";

export const DisplayChart = ({
  duration,
  game_data,
  // display_players,
  display_chart,
}: {
  duration: number;
  game_data: game_summary;
  // display_players: string[];
  display_chart: string;
}) => {
  const players_data = game_data.players;
  const players = game_data.players.map((p) => p.name);
  const [display_players, setDisplayPlayers] =
    useState<(string | number)[]>(players);
  const [chartData, setChartData] = useState<(string | number)[][]>([]);
  const [valueType, setValueType] = useState<valueType>("cost");
  const [valueMeasure, setValueMeasure] = useState<valueMeasureType>("balance");

  // get the list of pbgids
  const charts_detail_Query = useQuery<
    AxiosResponse<chart_json, any>,
    Error,
    AxiosResponse<chart_json, any>,
    [string, string]
  >({
    queryKey: ["chart_detail", display_chart],
    queryFn: async ({ queryKey }) => {
      const [_key, id] = queryKey;
      return await jsonServerApi(`charts/${id}`);
    },
    enabled: !!display_players.length,
  });

  useEffect(() => {
    if (charts_detail_Query.data != undefined && display_players.length > 0) {
      const chart_details = charts_detail_Query.data.data.details;
      const pbgids_list = chart_details.map((d) => d.pbgid);
      // build head of data
      const data: (string | number)[][] = [data_head(display_players)];

      // for each second calculate value
      for (let i = 0; i <= duration; i++) {
        // for each player calculate value
        const display_players_data = extract_display_players_data(
          players_data,
          display_players
        );

        const chart_values_for_second_i = display_players_data.map(
          (player, index) => {
            // const log = i == 0 && index == 0;

            // find items that we want to calculate
            const buildOrders = extract_buildOrders(
              pbgids_list,
              player.buildOrder
            );

            // for each item calculate the value - count or costs -- value could be count or cost
            const value = calculate_buildOrder_value(
              i,
              buildOrders,
              valueType,
              valueMeasure,
              chart_details
            );

            // merge the calculated items
            return sum(value);
          }
        );

        // push it to data
        // `${Math.floor(i/60)}:${i % 60}`
        data.push([i, ...chart_values_for_second_i]);
      }

      setChartData(data);
    }
  }, [
    display_chart,
    display_players,
    charts_detail_Query.data,
    valueType,
    valueMeasure,
  ]);

  if (!display_players.length) {
    return <Text>please select a player to display</Text>;
  }

  if (charts_detail_Query.isLoading) {
    return (
      <Center>
        <Spinner />
      </Center>
    );
  }

  if (charts_detail_Query.isError) {
    return (
      <Center>
        <Text>error occured in getting charts detail</Text>
      </Center>
    );
  }

  if (charts_detail_Query.data == undefined) {
    return (
      <Center>
        <Text>this chart had not build completely</Text>
      </Center>
    );
  }

  return (
    <Flex
      w={"90vw"}
      margin={"24px"}
      background={"linear-gradient(#394766, #1e2536)"}
      flexDir={"column"}
      padding={"0 24px 24px"}
    >
      <Chart
        chartType="LineChart"
        height="400px"
        data={chartData}
        className="chart"
        options={{
          curveType: "function",
          legend: "bottom",
          backgroundColor: "transparent",
          hAxis: {
            minValue: 0,
            // ticks: timeTick,
            title: "time",
          },
          vAxis: {
            minValue: 0,
            title: `${charts_detail_Query.data.data.name} ${valueMeasure} - ${valueType}`,
          },
        }}
      />
      <Flex alignItems={"center"} justifyContent={"space-between"} mt={"12px"}>
        <DisplayPlayerFilter
          game_data={game_data}
          display_players={display_players}
          setDisplayPlayers={setDisplayPlayers}
        />
        <Flex flexDir={"column"} gap={"12px"}>
          <RadioGroup
            value={valueType}
            onChange={(e: valueType) => setValueType(e)}
          >
            <Flex gap={"24px"}>
              <Text>value type</Text>
              <Radio value="count">count</Radio>
              <Radio value="cost">cost</Radio>
            </Flex>
          </RadioGroup>
          <RadioGroup
            value={valueMeasure}
            onChange={(e: valueMeasureType) => setValueMeasure(e)}
          >
            <Flex gap={"24px"}>
              <Text>value measure</Text>
              <Radio value="balance">balance</Radio>
              <Radio value="product">product</Radio>
              <Radio value="lost">lost</Radio>
            </Flex>
          </RadioGroup>
        </Flex>
      </Flex>
    </Flex>
  );
};

const DisplayPlayerFilter = ({
  game_data,
  display_players,
  setDisplayPlayers,
}: {
  game_data: game_summary;
  display_players: (string | number)[];
  setDisplayPlayers: Dispatch<SetStateAction<(string | number)[]>>;
}) => {
  const teams = uniq(game_data.players.map((p) => p.teamName).sort())
  console.log("teams", teams)
  const team_players = teams.map((teamName) => ({
    teamName: teamName,
    players: game_data.players.filter((v) => v.teamName == teamName),
  }));
  return (
    <>
      {/* gap between team columns */}
      <Flex gap={"36px"} flexWrap={"wrap"}>
        <CheckboxGroup
          value={display_players}
          onChange={(e) => {
            if (e.length == 0) {
              setDisplayPlayers(game_data.players.map((p) => p.name));
            } else {
              setDisplayPlayers(e);
            }
          }}
        >
          {/* for each team we have a column */}
          {team_players.map((t) => (
            <Flex key={t.teamName} flexDir={"column"} gap={"12px"}>
              <Heading size={"small"}>{t.teamName}</Heading>
              {t.players.map((p) => (
                <Checkbox key={p.profileId} value={p.name}>
                  {p.name}
                </Checkbox>
              ))}
            </Flex>
          ))}
        </CheckboxGroup>
      </Flex>
    </>
  );
};
