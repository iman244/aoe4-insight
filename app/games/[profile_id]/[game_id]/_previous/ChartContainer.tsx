import { Text } from "@chakra-ui/react";
import { DisplayChart } from "./DisplayChart";
import game_summary from "@/types/aoe4_api/entities/summary/game_replay_summary";

export const ChartContainer = ({
  duration,
  game_data,
  display_chart,
}: {
  duration: number;
  game_data: game_summary;
  display_chart: string | undefined;
}) => {


  if (display_chart == undefined) {
    return <Text>please select a chart</Text>;
  }

  return (
    <DisplayChart
      duration={duration}
      game_data={game_data}
      display_chart={display_chart}
    />
  );
};
