import jsonServerApi from "@/app/utilities/jsonServerApi";
import {
  Button,
  Center,
  Flex,
  FormErrorMessage,
  Heading,
  Icon,
  Link,
  Text,
  Textarea,
  useToast,
} from "@chakra-ui/react";
import { useMutation } from "@tanstack/react-query";
import { FormEventHandler, useState } from "react";
import { FaExternalLinkAlt } from "react-icons/fa";

export const UploadJSON = ({
  profile_id,
  game_id,
  refresh_query
}: {
  profile_id: string;
  game_id: string;
  refresh_query: () => void;
}) => {
  const [json, setJson] = useState("");
  const [jsonError, set_jsonError] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const toast = useToast();

  const uploadJson = useMutation({
    mutationFn: async ({ game_data }: any) => {
      return await jsonServerApi.post("games", game_data);
    },
    onSuccess: (data) => {
      setIsLoading(false);
      console.log("data", data)
      toast({
        status: "success",
        position: "bottom",
        size: 'sm',
        description:
          "we received your game data!",
      });
      refresh_query()
    },
  });

  const handleSubmit: FormEventHandler<HTMLFormElement> = (event) => {
    event.preventDefault();

    try {
      let game_data = JSON.parse(json);
      game_data.id = game_id;
      set_jsonError("");
      const upload = uploadJson.mutate({
        game_data,
      });
      setIsLoading(true);
    } catch (error: any) {
      set_jsonError(error.message);
    }
  };

  return (
    <Center minH={"100vh"} flexDir={"column"} gap={"48px"} padding={"48px"}>
      <Button
        as={Link}
        href={`https://aoe4world.com/players/${profile_id}/games/${game_id}`}
        isExternal
        alignSelf={"center"}
      >
        please retrive and upload game data from aoe4world game page
        <Icon as={FaExternalLinkAlt} boxSize={3} ml={"8px"} />
      </Button>
      <form onSubmit={handleSubmit} style={{ width: "100%" }}>
        <Flex flexDir={"column"} gap={"12px"}>
          <Heading
            borderBottom={"1px solid gray"}
            w={"fit-content"}
            paddingBottom={"4px"}
          >
            Upload json data
          </Heading>
          {jsonError && (
            <Text fontSize={"small"} color={"red.300"}>
              {jsonError}
            </Text>
          )}
          <Textarea
            w={"100%"}
            value={json}
            onChange={(e) => setJson(e.target.value)}
            placeholder="please add your json data that you retrive from aoe4world summary page"
          />
          <Button type="submit" isLoading={isLoading} isDisabled={isLoading}>
            Save My Game
          </Button>
        </Flex>
      </form>
    </Center>
  );
};
