import jsonServerApi from "@/app/utilities/jsonServerApi";
import {
  Button,
  Center,
  Flex,
  FormErrorMessage,
  Heading,
  Icon,
  Link,
  Text,
  Textarea,
  useToast,
} from "@chakra-ui/react";
import { useMutation } from "@tanstack/react-query";
import { FormEventHandler, useEffect, useState } from "react";
import { FaExternalLinkAlt } from "react-icons/fa";

export const UploadJsonToIndexDb = ({
  profile_id,
  game_id,
}: {
  profile_id: string;
  game_id: string;
}) => {
  const log = process.env.NEXT_PUBLIC_ENVIRONMENT != 'production'
  const [json, setJson] = useState("");
  const [jsonError, set_jsonError] = useState("");
  const [jsonSuccess, set_jsonSuccess] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const toast = useToast();
  const [db, setDB] = useState<IDBDatabase | null>(null);
  // const [transaction, setTransaction] = useState<IDBOpenDBRequest | null>(null);

  useEffect(() => {
    const idb_request = window.indexedDB.open("aoe4-insight", 2);

    idb_request.onsuccess = (ev) => {
      if (idb_request) {
        setDB(idb_request.result);
      }
    };

    idb_request.onerror = (ev) => {
      // One of the common possible errors when opening a database is VER_ERR.
      // It indicates that the version of the database stored on the disk is
      // greater than the version that you are trying to open. This is an error
      // case that must always be handled by the error handler.
      set_jsonError("indexdb could not openned: " + ev.type);
    };

    idb_request.onupgradeneeded = (ev: IDBVersionChangeEvent) => {
      if(idb_request) {
        const db = idb_request.result
        const GamesObjectStore = db.createObjectStore("games", { keyPath: "gameId" });

        // const GamePlayersObjectStore = db.createObjectStore("game_players", { autoIncrement: true });
        // GamePlayersObjectStore.createIndex('profileId', 'profileId', {unique: false})
        // GamePlayersObjectStore.createIndex('name', 'name', {unique: false})

        // const GamePlayerBuildOrdersObjectStore = db.createObjectStore("game_player_buildorders", { autoIncrement: true });
        // GamePlayerBuildOrdersObjectStore.createIndex('id', 'id', {unique: false})
        // GamePlayerBuildOrdersObjectStore.createIndex('pbgid', 'pbgid', {unique: false})
        // GamePlayerBuildOrdersObjectStore.createIndex('type', 'type', {unique: false})
      }

    }
  }, []);

  useEffect(() => {
    log && log && console.log("db", db);
  }, [db]);

  const handleSubmit: FormEventHandler<HTMLFormElement> = (event) => {
    event.preventDefault();

    if(json.length == 0) {
      set_jsonError("Please enter your data")
      return;
    }

    if (db == null) {
      set_jsonError("index db dose not openned");
      return;
    }

    try {
      const transaction = db.transaction(["games"], "readwrite");
      transaction.oncomplete = (event) => {
        log && log && console.log("Index db transaction success event: ", event)
        set_jsonSuccess("Transaction done!");
      };
      
      transaction.onerror = (event) => {
        log && console.log("Index db transaction Error event: ", event)
        set_jsonError('Index db transaction Error')
      };
      

      const objectStore = transaction.objectStore("games");

      const request = objectStore.add(JSON.parse(json));

      request.onsuccess = (event) => {
        // event.target.result === customer.ssn;
        log && console.log("request.onsuccess event", event)
        set_jsonSuccess("Data added successfully");
      };

      request.onerror = (event) => {
        // event.target.result === customer.ssn;
        log && console.log("request.onerror event", event)
        set_jsonError("Data did not added");
      };

    } catch (error) {
      log && console.log("error", error);
      if (error instanceof DOMException) {
        if (error.name == "NotFoundError") {
          db.createObjectStore("summary_game", {
            keyPath: "summary_game",
          });
        }
      }
    }
  };

  return (
    <Center minH={"100vh"} flexDir={"column"} gap={"48px"} padding={"48px"}>
      <Button
        as={Link}
        href={`https://aoe4world.com/players/${profile_id}/games/${game_id}`}
        isExternal
        alignSelf={"center"}
      >
        please retrive and upload game data from aoe4world game page
        <Icon as={FaExternalLinkAlt} boxSize={3} ml={"8px"} />
      </Button>
      <form onSubmit={handleSubmit} style={{ width: "100%" }}>
        <Flex flexDir={"column"} gap={"12px"}>
          <Heading
            borderBottom={"1px solid gray"}
            w={"fit-content"}
            paddingBottom={"4px"}
          >
            Upload json data
          </Heading>
          {jsonError && (
            <Text fontSize={"small"} color={"red.300"}>
              {jsonError}
            </Text>
          )}
          {jsonSuccess && (
            <Text fontSize={"small"} color={"green.300"}>
              {jsonSuccess}
            </Text>
          )}
          <Textarea
            w={"100%"}
            value={json}
            onChange={(e) => setJson(e.target.value)}
            placeholder="please add your json data that you retrive from aoe4world summary page"
          />
          <Button type="submit" isLoading={isLoading} isDisabled={isLoading}>
            Save My Game
          </Button>
        </Flex>
      </form>
    </Center>
  );
};
