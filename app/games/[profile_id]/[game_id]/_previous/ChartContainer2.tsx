import game_summary from "@/types/aoe4_api/entities/summary/game_replay_summary";
import { DisplayChart2 } from "./DisplayChart2";
import { Text } from "@chakra-ui/react";

export const ChartContainer2 = ({
  duration,
  game_data,
  display_chart,
}: {
  duration: number;
  game_data: game_summary;
  display_chart: number | undefined;
}) => {

  if (display_chart == undefined) {
    return <Text>please select a chart</Text>;
  }

  return <DisplayChart2 duration={duration} game_data={game_data} display_chart={display_chart} />;
};
