"use client";
import jsonServerApi from "@/app/utilities/jsonServerApi";
import { Button, Center, Flex, Heading, Spinner, Text } from "@chakra-ui/react";
import { useQuery } from "@tanstack/react-query";
import { UploadJSON } from "./UploadJSON";
import { Dispatch, SetStateAction, useState, use } from "react";
import { GameInfo } from "./GameInfo";
import { AoeHeading } from "@/app/components/AoeHeading";
import { AxiosResponse } from "axios";
import game_summary from "@/types/aoe4_api/entities/summary/game_replay_summary";
import { SelectPlayers } from "@/app/components/ReactSelect/SelectPlayers";
import { SelectCharts } from "@/app/components/ReactSelect/SelectCharts";
import Chart from "react-google-charts";
import { DisplayChart } from "./DisplayChart";
import Link from "next/link";
import { ChartContainer } from "./ChartContainer";
import { ChartContainer2 } from "./ChartContainer2";
import { ChartsListContextProvider } from "@/app/admin/ChartsList/ChartsListContextProvider";

export type multiSelect = { value: string; label: string }[];
export type multiSelectState = [
  multiSelect,
  Dispatch<SetStateAction<multiSelect>>
];

export default function Game(
  props: {
    params: Promise<{ game_id: string; profile_id: string }>;
  }
) {
  const params = use(props.params);
  const selectPlayerState = useState<{ value: string; label: string }[]>([]);
  const selectedChartState = useState<{ value: string; label: string } | null>(
    null
  );
  const { game_id, profile_id } = params;
  const [queryKey, setQueryKey] = useState(0);
  const refresh_query = () => setQueryKey((pre) => pre + 1);

  const gameDataQuery = useQuery<
    AxiosResponse<game_summary, any>,
    Error,
    AxiosResponse<game_summary, any>,
    [string, string, number]
  >({
    queryKey: ["game", game_id, queryKey],
    queryFn: async ({ queryKey }) => {
      const [_key, game_id] = queryKey;
      return await jsonServerApi(`games/${game_id}`);
    },
    retry: 1,
  });

  if (gameDataQuery.isLoading) {
    return (
      <Center minH={"100vh"}>
        <Spinner />
      </Center>
    );
  }

  if (gameDataQuery.isError) {
    return (
      <UploadJSON
        profile_id={profile_id}
        game_id={game_id}
        refresh_query={refresh_query}
      />
    );
  }

  if (gameDataQuery.data) {
    return (
      <ChartsListContextProvider>
        <Center flexDir={"column"} gap={"24px"}>
          <GameInfo game={gameDataQuery.data.data} />
          <AoeHeading fontSize="xx-large">Charts</AoeHeading>

          {/* <AoeHeading fontSize="xx-large">
            classic age of empires charts
          </AoeHeading> */}
          <Flex alignItems={"center"} justifyContent={"center"} gap={"24px"}>
            <SelectCharts selectedChartState={selectedChartState} />
            <Button
              as={Link}
              href={"/admin"}
              colorScheme="blue"
              size={"sm"}
              target="_blank"
            >
              Create More Charts
            </Button>
          </Flex>

          <ChartContainer
            duration={gameDataQuery.data.data.duration}
            game_data={gameDataQuery.data.data}
            display_chart={selectedChartState[0]?.value}
          />
                    <ChartContainer2
            duration={gameDataQuery.data.data.duration}
            game_data={gameDataQuery.data.data}
            display_chart={Number(selectedChartState[0]?.value)}
          />
        </Center>
      </ChartsListContextProvider>
    );
  }
}
