export interface Game {
    gameId: number
    winReason: string
    mapId: number
    mapName: string
    mapSize: string
    mapSizeMaxPlayers: number
    mapBiome: string
    mapSeed: string
    leaderboard: string
    duration: number
    startedAt: number
    finishedAt: number
    spectatorsCount: number
    players: Player[]
  }
  
  export interface Player {
    profileId: number
    name: string
    civilization: string
    team: number
    teamName: string
    apm: number
    result: string
    _stats: Stats
    civilizationAttrib: string
    actions: Actions
    scores: Scores
    totalResourcesGathered: TotalResourcesGathered
    totalResourcesSpent: TotalResourcesSpent
    resources: Resources
    buildOrder: BuildOrder[]
  }
  
  export interface Stats {
    abil: number
    bprod: number
    edeaths: number
    ekills: number
    elitekill: number
    gt: number
    inactperiod: number
    sqkill: number
    sqlost: number
    sqprod: number
    structdmg?: number
    totalcmds: number
    unitprod: number
    upg: number
    blost?: number
  }
  
  export interface Actions {
    pickupRelic?: number[]
    feudalAge: number[]
    castleAge: number[]
    imperialAge?: number[]
    upgradeUnitCrossbowman4?: number[]
    upgradeRangedIncendiary?: number[]
    upgradeRangedArcherSilkstring?: number[]
    upgradeSiegeEngineers?: number[]
    upgradeEconResourceFoodHarvestRate2: number[]
    upgradeEconResourceWoodHarvestRate2: number[]
    upgradeTechMilitaryAcademy?: number[]
    upgradeUnitTownCenterWheelbarrow1: number[]
    upgradeUnitArcher3Eng?: number[]
    upgradeUnitArcher4Eng?: number[]
    upgradeUnitManatarms2Eng?: number[]
    upgradeUnitManatarms3Eng?: number[]
    upgradeUnitManatarms4Eng?: number[]
    upgradeUnitSpearman3Eng?: number[]
    upgradeEconResourceFoodHarvestRate3?: number[]
    upgradeEconResourceGoldHarvestRate2: number[]
    upgradeEconResourceGoldHarvestRate3?: number[]
    upgradeEconResourceWoodHarvestRate3?: number[]
    upgradeEconResourceWoodFellRate1?: number[]
    upgradeMeleeDamageI: number[]
    upgradeMeleeDamageIi: number[]
    upgradeMeleeArmorI?: number[]
    upgradeMeleeArmorIi?: number[]
    upgradeRangedArmorI: number[]
    upgradeRangedArmorIi: number[]
    upgradeRangedArmorIii?: number[]
    upgradeRangedDamageI: number[]
    upgradeRangedDamageIi?: number[]
    upgradeRangedDamageIii?: number[]
    upgradeManatarmsEliteArmyTactics?: number[]
    upgradeRangedLongbowArrowVolleyEng?: number[]
    upgradeFarmImprovedEnclosuresEng?: number[]
    upgradeTrebuchetAoeEng?: number[]
    upgradeArmorCladEng?: number[]
    upgradeNetworkOfCitadelsEng?: number[]
    upgradeVillagerHealth?: number[]
    upgradeAbbeyKingCastle1?: number[]
    upgradeAbbeyKingImp2?: number[]
    upgradeUnitSpearman3?: number[]
    upgradeUnitHorseman3?: number[]
    upgradeUnitSpearman2?: number[]
    upgradeKeepSpringald?: number[]
    upgradeUnitKnight3Fre?: number[]
    upgradeCavalryChivalryFre?: number[]
    upgradeUnitArcher3?: number[]
    upgradeEconVillagerHuntingGear1?: number[]
    monkStatetreeDepositRelic?: number[]
    monkStatetreeDropRelic?: number[]
    monkRemoveRelic?: number[]
    upgradeUnitSpearman4?: number[]
    upgradeTechUniversityMurderHoles4?: number[]
    upgradeUnitKnight4Fre?: number[]
    upgradeCavalryCantledSaddleFre?: number[]
    upgradeEconResourceFoodHarvestRate4?: number[]
    upgradeEconResourceGoldHarvestRate4?: number[]
    upgradeKeepCannon?: number[]
    upgradeMeleeDamageIii?: number[]
    upgradeEnlistmentIncentivesFre?: number[]
    upgradeScoutLosIncrease?: number[]
  }
  
  export interface Scores {
    total: number
    military: number
    economy: number
    technology: number
    society: number
  }
  
  export interface TotalResourcesGathered {
    food: number
    gold: number
    stone: number
    wood: number
    oliveoil: number
    total: number
  }
  
  export interface TotalResourcesSpent {
    food: number
    gold: number
    stone: number
    wood: number
    oliveoil: number
    total: number
  }
  
  export interface Resources {
    timestamps: number[]
    food: number[]
    gold: number[]
    stone: number[]
    wood: number[]
    foodPerMin: number[]
    goldPerMin: number[]
    stonePerMin: number[]
    woodPerMin: number[]
    foodGathered: number[]
    goldGathered: number[]
    stoneGathered: number[]
    woodGathered: number[]
    total: number[]
    military: number[]
    economy: number[]
    technology: number[]
    society: number[]
  }
  
  export interface BuildOrder {
    id: string
    icon: string
    pbgid: number
    modid: any
    type: string
    finished: number[]
    constructed: number[]
    packed: any[]
    unpacked: any[]
    transformed: any[]
    destroyed: number[]
    unknown: Unknown
  }
  
  export interface Unknown {
    "6"?: number[]
    "10"?: number[]
  }
  