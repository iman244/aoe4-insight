import { useEffect, useState } from 'react'
import { Status } from './useGame';

export const useIndexdb = (name: string, version?: number) => {
  
    // const [db, setDB] = useState<IDBDatabase | null>(null);
    // const [error, setError] = useState<Event | null>(null)
    const [result, setResult] = useState<{
      db: IDBDatabase | null;
      error: Event | null;
      status: Status;
    }>({
      db: null,
      error: null,
      status: 'idle'
    })

    useEffect(() => {
      setResult((pre)=>({...pre, status: 'loading'}))
      const idb_request = window.indexedDB.open(name, version);
  
      idb_request.onsuccess = (ev) => {
        if (idb_request) {
          setResult({
            status: 'success',
            error: null,
            db: idb_request.result,
          });
        }
      };
  
      idb_request.onerror = (ev) => {
        // One of the common possible errors when opening a database is VER_ERR.
        // It indicates that the version of the database stored on the disk is
        // greater than the version that you are trying to open. This is an error
        // case that must always be handled by the error handler.
        setResult({
          status: 'error',
          error: ev,
          db: null,
        });
      };
  
      idb_request.onupgradeneeded = (ev: IDBVersionChangeEvent) => {
        if (idb_request) {
          const db = idb_request.result;
          const GamesObjectStore = db.createObjectStore("games", {
            keyPath: "gameId",
          });
        }
      };
    }, []);

  return result
}
