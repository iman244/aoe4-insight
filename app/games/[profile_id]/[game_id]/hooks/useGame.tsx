import { useEffect, useState } from "react";
import { Game } from "../type";

export type Status = "success" | "error" | "loading" | "idle";

export const useGame = (
  db: IDBDatabase | null,
  { game_id }: { game_id: number | string }
): {
  game: Game | null;
  status: Status;
  error: Event | string | null;
} => {
  const log = process.env.NEXT_PUBLIC_ENVIRONMENT != "production";

  // const [game, setGame] = useState<Game | null>(null);
  // const [error, setError] = useState<Event | null>(null);
  // const [status, setStatus] = useState<Status>("loading");
  const [result, setResult] = useState<{
    game: Game | null;
    error: Event | null;
    status: Status;
  }>({
    game: null,
    error: null,
    status: "idle",
  });

  useEffect(() => {
    log && console.log("db", db);
    if (db) {
      setResult((pre)=>({
        ...pre,
        status: 'loading'
      }));
      // loading previous game data
      const transaction = db.transaction(["games"], "readonly");
      log && console.log("loading previous game data transaction", transaction);

      const objectStore = transaction.objectStore("games");
      const request = objectStore.get(Number(game_id));

      request.onsuccess = (ev) => {
        log &&
          console.log("objectStore.get(Number(game_id)) request onsuccess");

          setResult({
          error: null,
          game: request.result,
          status: 'success'
        })
      };
      request.onerror = (ev) => {
        log && console.log("objectStore.get(Number(game_id)) request onerror", ev);
 
        setResult({
          status: 'error',
          error: ev,
          game: null
        })
      };
    }
  }, [db]);

  if (!db) {
    log && console.log("game could not return because db is null");
    return {
      game: null,
      status: "idle",
      error: "db is null",
    };
  }

  return result
};
