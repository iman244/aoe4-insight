"use client";
import games_list_api from "@/types/aoe4_api/api_list/games_list";
import { Box, Center, Flex, Icon, Spinner, Text } from "@chakra-ui/react";
import { useQuery } from "@tanstack/react-query";
import axios, { AxiosResponse } from "axios";
import GameCover from "./gameCover";
import Pagination from "rc-pagination/lib/Pagination";
import en_US from "rc-pagination/es/locale/en_US";
import { useState, use } from "react";
import Select from "react-select";
import PlayerName from "./PlayerName";
import DropdownIndicator from "@/app/components/ReactSelect/DropdownIndicator";
import aoe4SelectTheme from "../../components/ReactSelect/aoe4SelectTheme";
import aoe4SelectStyle from "@/app/components/ReactSelect/aoe4SelectStyle";
import leaderboardOptions from "./leaderboardOptions";

export default function GamesList(
  props: {
    params: Promise<{ profile_id: string }>;
  }
) {
  const params = use(props.params);
  const profile_id = Number(params.profile_id);

  const [page, setPage] = useState(1);
  const [leaderboard, setLeaderboard] = useState("");
  const limit = 20;

  const gamesQuery = useQuery<
    AxiosResponse<games_list_api, any>,
    Error,
    AxiosResponse<games_list_api, any>,
    [string, number, string]
  >({
    queryKey: ["games", page, leaderboard],
    queryFn: async ({ queryKey }) => {
      const [_key, page, leaderboard] = queryKey;
      return await axios.get(
        `https://aoe4world.com/api/v0/players/${profile_id}/games?page=${page}&limit=${limit}&leaderboard=${leaderboard}`
      );
    },
  });

  return (
    <Center
      as={"main"}
      minH={"100vh"}
      margin={"48px"}
      flexDir={"column"}
      gap={"48px"}
    >
      <Flex flexDir={"column"} gap={"24px"}>
        {!!gamesQuery.data && (
          <PlayerName
            profile_id={profile_id}
            game={gamesQuery.data.data.games[0]}
          />
        )}
        <Select
          placeholder="Select leaderboard"
          components={{ DropdownIndicator }}
          theme={aoe4SelectTheme}
          styles={aoe4SelectStyle}
          options={leaderboardOptions}
          isClearable
          onChange={(newValue: any)=>{
            setLeaderboard(()=>newValue ? newValue.value : '')
          }}
        />
      </Flex>

      {gamesQuery.data ? <Flex flexWrap={"wrap"} gap={"36px"} justifyContent={"center"}>
        {gamesQuery.data?.data.games.map((game) => (
          <GameCover key={game.game_id} profile_id={profile_id} game={game} />
        ))}
      </Flex> :  <Spinner size={'lg'} color="#e7c067" />}
      {gamesQuery.data && (
        <Pagination
          defaultCurrent={1}
          current={page}
          pageSize={1}
          total={Math.floor(gamesQuery.data.data.total_count / limit) + 1}
          showTitle
          showLessItems
          locale={en_US}
          prevIcon={PaginateIcon}
          nextIcon={PaginateIcon}
          onChange={(page, pageSize) => {
            console.log(page, pageSize);
            setPage(page);
          }}
        />
      )}
    </Center>
  );
}

const PaginateIcon = () => {
  return (
    <Box
      backgroundColor={"#e7c067"}
      w={"24px"}
      height={"24px"}
      _hover={{ cursor: "pointer" }}
    />
  );
};
