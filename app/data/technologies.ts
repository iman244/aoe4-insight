export interface TechnologiesData {
  __note__: string;
  __version__: string;
  data: Daum[];
}

export interface Daum {
  id: string;
  baseId: string;
  type: string;
  name: string;
  pbgid: number;
  attribName: string;
  age: number;
  civs: string[];
  description: string;
  classes: string[];
  displayClasses: string[];
  unique?: boolean;
  costs: Costs;
  producedBy: string[];
  icon: string;
  effects?: Effect[];
  unlocks?: string;
  active?: string;
  auraRange?: number;
}

export interface Costs {
  food: number;
  wood: number;
  stone: number;
  gold: number;
  vizier: number;
  oliveoil: number;
  total: number;
  popcap: number;
  time?: number;
}

export interface Effect {
  property: string;
  select?: Select;
  effect: string;
  value?: number;
  type: string;
  target?: Target;
}

export interface Select {
  id?: string[];
  class?: string[][];
}

export interface Target {
  class: string[][];
}
