export interface BuildingsData {
    __note__: string
    __version__: string
    data: Daum[]
  }
  
  export interface Daum {
    id: string
    baseId: string
    type: string
    name: string
    pbgid: number
    attribName: string
    age: number
    civs: string[]
    description: string
    classes: string[]
    displayClasses: string[]
    unique: boolean
    costs: Costs
    producedBy: string[]
    icon: string
    hitpoints: number
    weapons: Weapon | undefined[]
    armor: Armor[]
    sight: Sight
    influences: string[]
    garrison?: Garrison
  }
  
  export interface Costs {
    food: number
    wood: number
    stone: number
    gold: number
    vizier: number
    oliveoil: number
    total: number
    popcap: number
    time: number
  }
  
  export interface Weapon {
    name: string
    type: string
    damage: number
    speed: number
    range: Range
    modifiers: Modifier[]
    durations: Durations
    attribName: string
    pbgid: number
    burst?: Burst
  }
  
  export interface Range {
    min: number
    max: number
  }
  
  export interface Modifier {
    property: string
    target: Target
    effect: string
    value: number
    type: string
  }
  
  export interface Target {
    class: string[][]
  }
  
  export interface Durations {
    aim: number
    windup: number
    attack: number
    winddown: number
    reload: number
    setup: number
    teardown: number
    cooldown: number
  }
  
  export interface Burst {
    count: number
  }
  
  export interface Armor {
    type: string
    value: number
  }
  
  export interface Sight {
    inner_height: number
    inner_radius: number
    outer_height: number
    outer_radius: number
    base: number
    line: number
    height: number
  }
  
  export interface Garrison {
    capacity: number
    classes: string[]
  }
  