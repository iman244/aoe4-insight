import { valueMeasureType } from "@/types/aoe4_api/entities/chart/valueMeasureType";
import buildOrder from "@/types/aoe4_api/entities/summary/buildOrder";

export default function count_calculation(
  i: number,
  buildOrder: buildOrder,
  valueMeasure: valueMeasureType
) {
  var product_count =
    buildOrder.finished.filter((finished_time) => finished_time <= i).length +
    buildOrder.constructed.filter((finished_time) => finished_time <= i).length;

  var lost_count = buildOrder.destroyed.filter(
    (destroyed_time) => destroyed_time <= i
  ).length;

  // fishing boat is calculated in unknown 14
  if (buildOrder.unknown != undefined) {
    if (
      buildOrder.unknown[14] != undefined &&
      Array.isArray(buildOrder.unknown[14])
    ) {
      product_count += buildOrder.unknown[14].filter(
        (finished_time) => finished_time <= i
      ).length;
    }
    if (
      buildOrder.unknown[15] != undefined &&
      Array.isArray(buildOrder.unknown[15])
    ) {
      lost_count += buildOrder.unknown[15].filter(
        (finished_time) => finished_time <= i
      ).length;
    }
  }

  switch (valueMeasure) {
    case "product":
      return product_count;
    case "lost":
      return lost_count;
    default:
      return product_count - lost_count;
  }
}
