import buildOrder from "@/types/aoe4_api/entities/summary/buildOrder";

export default function extract_buildOrders(pbgids_list: number[], buildOrders: buildOrder[]) {
  return buildOrders.filter((item) =>
    pbgids_list.includes(item.pbgid)
  );
}
