export default function data_head(data_head: (number | string)[]) {
    return ["time", ...data_head]
}
