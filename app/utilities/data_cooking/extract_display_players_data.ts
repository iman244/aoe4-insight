import player_replay_summary from "@/types/aoe4_api/entities/summary/player_replay_summary";

export default function extract_display_players_data(players_data: player_replay_summary[], display_players: (string | number)[]) {
  return players_data
  .filter((player_replay_summary) => {
    return display_players.includes(player_replay_summary.name);
  })
  .sort(
    (a, b) =>
      display_players.indexOf(a.name) - display_players.indexOf(b.name)
  );
}
