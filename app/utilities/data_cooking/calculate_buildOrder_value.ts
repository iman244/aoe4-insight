import { valueMeasureType } from "@/types/aoe4_api/entities/chart/valueMeasureType";
import { valueType } from "@/types/aoe4_api/entities/chart/valueType";
import buildOrder from "@/types/aoe4_api/entities/summary/buildOrder";
import count_calculation from "../count_calculation";
import cost_calculation from "../cost_calculation";
import chart_detail from "@/types/aoe4_api/entities/chart/chart_detail";
import extract_cost from "../extract_cost";

export default function calculate_buildOrder_value(
  second: number,
  buildOrders: buildOrder[],
  valueType: valueType,
  valueMeasure: valueMeasureType,
  chart_details: chart_detail[]
) {
  return buildOrders.map((buildOrder) => {
    switch (valueType) {
      case "count":
        return count_calculation(second, buildOrder, valueMeasure);
      case "cost":
        return cost_calculation(
          second,
          buildOrder,
          valueMeasure,
          extract_cost(chart_details, buildOrder.pbgid)
        );

      default:
        return 0;
    }
  });
}
