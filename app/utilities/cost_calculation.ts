import chart_detail from "@/types/aoe4_api/entities/chart/chart_detail";
import buildOrder from "@/types/aoe4_api/entities/summary/buildOrder";
import count_calculation from "./count_calculation";
import { valueMeasureType } from "@/types/aoe4_api/entities/chart/valueMeasureType";
import extract_cost from "./extract_cost";
import costs from "@/types/aoe4_api/entities/entity/fields/costs";

export default function cost_calculation(
  i: number,
  buildOrder: buildOrder,
  valueMeasure: valueMeasureType,
  costs: costs
) {
  var count = count_calculation(i, buildOrder, valueMeasure);


  return count * costs.total;
}
