import game from "@/types/aoe4_api/entities/game/game";
import player_in_game_details from "@/types/aoe4_api/entities/player/player_in_game_details";
import { team } from "@/types/aoe4_api/entities/team";

export function retrive_players(teams: team[]) {
  return teams.reduce(
    (previous_team: player_in_game_details[], current_team) => {
      const simple_team = current_team.map(
        (complex_team) => complex_team.player
      );

      return [...previous_team, ...simple_team];
    },
    []
  );
}

export function retrive_opponents_from_players(
  profile_id: number,
  players: player_in_game_details[]
) {
  return players.find(
    (player_in_game_details) => profile_id != player_in_game_details.profile_id
  )!;
}

export function retrive_player(
  profile_id: number,
  players: player_in_game_details[]
) {
  return players.find(
    (player_in_game_details) => profile_id == player_in_game_details.profile_id
  )!;
}



export function retrive_opponents_info_from_game(
  profile_id: number,
  game: game
) {
  const players = retrive_players(game.teams);

  return retrive_opponents_from_players(profile_id, players);
}

export default function retrive_player_info_from_game(
  profile_id: number,
  game: game
) {
  const players = retrive_players(game.teams);

  return retrive_player(profile_id, players);
}