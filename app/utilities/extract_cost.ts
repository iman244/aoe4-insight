import chart_detail from "@/types/aoe4_api/entities/chart/chart_detail";

export default function extract_cost(
  chart_details: chart_detail[],
  pbgid: number
) {
  const cost = chart_details.find((i) => i.pbgid == pbgid)?.costs;

  if (cost == undefined) {
    return {
      food: 0,
      wood: 0,
      stone: 0,
      gold: 0,
      vizier: 0,
      oliveoil: 0,
      total: 0,
      popcap: 0,
      time: 0,
    };
  } else {
    return cost;
  }
}
