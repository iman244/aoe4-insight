import axios from "axios";

export default axios.create({
    baseURL: "https://aoe4world.com/api/v0/",
})