export default function onlyUnique(value: any, index: number, array: any[]) {
  // console.log("onlyUnique value", value)
  // console.log("onlyUnique array.indexOf(value)", array.indexOf(value))
  // console.log("onlyUnique index", index)
  return array.indexOf(value) == index;
}

export function removeDuplicates(arr: any[]) {
  return arr.filter((item, index) => arr.indexOf(item) === index);
}
