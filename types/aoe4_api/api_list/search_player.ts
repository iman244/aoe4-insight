import player_general_info from "../entities/player/player_general_info";

export interface search_player_api {
  count: number;
  leaderboard: string;
  players: player_general_info[];
  query: string;
}
