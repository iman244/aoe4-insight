import filters from "../entities/filters";
import game from "../entities/game/game";

export default interface games_list_api {
  count: number;
  filters: filters;
  games: game[];
  offset: number;
  page: number;
  per_page: number;
  total_count: number;
}
