import Avatars from "../avatars";
import player_simple from "./player_simple";

export default interface player_general_info extends player_simple {
  rank: null;
  avatars: Avatars;
  country: string;
  games_count: null;
  last_game_at: null;
  losses_count: null;
  rank_level: null;
  rating: null;
  site_url: string;
  steam_id: string;
  streak: null;
  win_rate: null;
  wins_count: null;
}
