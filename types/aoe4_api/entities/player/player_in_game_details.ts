import player_simple from "./player_simple";

export default interface player_in_game_details extends player_simple {
  civilization: string;
  civilization_randomized: boolean;
  input_type: string;
  mmr: number | null;
  mmr_diff: number | null;
  rating: number | null;
  rating_diff: number | null;
  result: string;
}
