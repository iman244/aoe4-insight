import { leaderboard } from "../leaderboard";
import { team } from "../team";


export default interface game {
  average_mmr: number;
  average_mmr_deviation: number;
  average_rating: number;
  average_rating_deviation: number;
  duration: number;
  game_id: number;
  just_finished: boolean;
  kind: string;
  leaderboard: leaderboard;
  map: string;
  mmr_leaderboard: any;
  ongoing: boolean;
  patch: number;
  season: number;
  server: string;
  started_at: string;
  teams: team[];
  updated_at: string;
}
