export default interface Avatars {
  full: string;
  medium: string;
  small: string;
}
