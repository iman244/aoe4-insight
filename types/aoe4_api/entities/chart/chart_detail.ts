import costs from "../entity/fields/costs";

export default interface chart_detail {
  pbgid: number;
  costs: costs;
}
