import chart_detail from "./chart_detail";

export default interface chart {
  name: string;
  details: chart_detail[];
}
