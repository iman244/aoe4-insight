import base_entity from "./base_entity";

export default interface upgrade extends base_entity {
    unlocks: string;
}