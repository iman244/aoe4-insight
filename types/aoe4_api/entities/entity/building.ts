import armor from "./fields/armor";
import base_entity from "./base_entity";
import garrison from "./fields/garrison";
import sight from "./fields/sight";

export default interface building extends base_entity {
    influences: string[];
    armor?: armor[]
    garrison?: garrison;
    sight?: sight;
}