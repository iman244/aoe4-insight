import costs from "./costs";

export default interface base_variation {
  id: string;
  pbgid: number;
  attribName: string;
  civs: [string];
  costs?: costs
}
