import range from "./range";

export default interface weapon {
  name: string;
  type: string;
  damage: number;
  speed: number;
  range: range;
  modifiers: any[];
  durations: any;
  attribName: string;
  pbgid: number;
}
