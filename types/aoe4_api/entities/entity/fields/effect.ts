export default interface effect {
  property: string;
  select: {
    id: string[];
  };
  effect: string;
  value: number;
  type: string;
}
