import base_entity from "./base_entity";
import movement from "./fields/movement";
import sight from "./fields/sight";
import weapon from "./fields/weapon";

export default interface unit extends base_entity {
  hitpoints: number;
  sight:sight;
  movement: movement;
  weapons?: weapon[];
}
