import base_entity from "./base_entity";
import effect from "./fields/effect";

export default interface abilitiy extends base_entity {
  active?: string;
  auraRange?: number;
  effects?: effect[];
  cooldown?: number;
}
