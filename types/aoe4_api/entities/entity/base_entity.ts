import costs from "./fields/costs";
import base_variation from "./fields/variations";

export default interface base_entity {
  id: string;
  name: string;
  type: "ability" | "building" | "technology" | "unit" | "upgrade";
  civs: string[];
  unique: boolean;
  displayClasses: string[];
  classes: string[];
  minAge: number;
  icon: string;
  description: string;
  variations: base_variation[];
  baseId?: string;
  age: number;
  costs: costs;
  producedBy: string[];
  shared: any;
}
