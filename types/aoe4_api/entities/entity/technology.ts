import base_entity from "./base_entity";
import effect from "./fields/effect";

export default interface technology extends base_entity {
    effects: effect[];
}