import buildOrder from "./buildOrder";
import resources_replay_summary from "./resources_replay_sumary";
import scores from "./scores";
import _stats from "./_stats";

export default interface player_replay_summary {
    actions: any;
    apm: number;
    buildOrder: buildOrder[];
    civilization: string;
    civilizationAttrib: string;
    name: string;
    profileId: number;
    resources: resources_replay_summary;
    result: "win" | "loss";
    scores: scores;
    team: number;
    teamName: string;
    _stats: _stats;
}
