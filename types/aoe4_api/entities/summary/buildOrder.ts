export default interface buildOrder {
  constructed: number[];
  destroyed: number[];
  finished: number[];
  icon: string;
  id: string;
  modid: null;
  packed: number[];
  pbgid: number;
  transformed: number[];
  type: "Unit" | "Animal" | "Building" | "Age" | "Upgrade";
  unknown: {14?: number[], 15?: number[]} | undefined;
  unpacked: [];
}
