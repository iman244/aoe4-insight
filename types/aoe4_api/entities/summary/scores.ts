export default interface scores {
  economy: number;
  military: number;
  society: number;
  technology: number;
  total: number;
}
