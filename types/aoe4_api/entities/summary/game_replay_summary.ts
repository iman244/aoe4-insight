import player_replay_summary from "./player_replay_summary";

export default interface game_summary {
  duration: number;
  finishedAt: number;
  gameId: number;
  id: string;
  leaderboard: string;
  mapBiome: string;
  mapId: number;
  mapName: string;
  mapSeed: string;
  mapSize: string;
  mapSizeMaxPlayers: number;
  spectatorsCount: number;
  startedAt: number;
  winReason: string;
  _recentGameHash: any;
  players: player_replay_summary[];
}
