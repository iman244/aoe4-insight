export default interface resources_replay_summary {
  economy: number[];
  food: number[];
  foodPerMin: number[];
  gold: number[];
  goldPerMin: number[];
  military: number[];
  society: number[];
  stone: number[];
  stonePerMin: number[];
  technology: number[];
  timestamps: number[];
  total: number[];
  wood: number[];
  woodPerMin: number[];
}
