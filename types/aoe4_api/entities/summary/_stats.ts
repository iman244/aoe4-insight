export default interface _stats {
  abil: number;
  bprod: number;
  edeaths: number;
  ekills: number;
  elitekill: number;
  gt: number;
  inactperiod: number;
  sqkill: number;
  sqlost: number;
  sqprod: number;
  totalcmds: number;
  unitprod: number;
  upg: number;
}
