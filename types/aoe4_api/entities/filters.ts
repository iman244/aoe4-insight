import { leaderboard } from "./leaderboard";

export default interface filters {
  leaderboard: leaderboard | null;
  opponent_profile_id: null;
  opponent_profile_ids: null;
  profile_ids: number[];
  since: null;
}
